# README

This is the smallest-possible example program using libopencm3.

It's intended for the ST Nucleo-F412ZG eval board. It should blink
the BLUE LED on the board.

## Board connections

*none required*
