/*
* pi.c
* Neil Gershenfeld 2/6/11
* STM32F412 port Zach Fredin 10/8/2019
* pi calculation benchmark
* pi = 3.14159265358979323846
*/

/*
#include <stdio.h>
#include <time.h>

#define NPTS 1000000000

void main() {
   int i;
   double a,b,c,pi,dt,mflops;
   struct timespec tstart,tend;
   clock_gettime(CLOCK_REALTIME,&tstart);
   a = 0.5;
   b = 0.75;
   c = 0.25;
   pi = 0;
   pi = 0;
   for (i = 1; i <= NPTS; ++i)
      pi += a/((i-b)*(i-c));
   clock_gettime(CLOCK_REALTIME,&tend);
   dt = (tend.tv_sec+tend.tv_nsec/1e9)-(tstart.tv_sec+tstart.tv_nsec/1e9);
   mflops = NPTS*5.0/(dt*1e6);
   printf("NPTS = %d, pi = %f\n",NPTS,pi);
   printf("time = %f, estimated MFlops = %f\n",dt,mflops);
}
*/

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#define NPTS 1000000

static void clock_setup(void){
	//no built-in libopencm3 support for a 100 MHz clock
	rcc_clock_setup_pll(&rcc_hsi_configs[RCC_CLOCK_3V3_84MHZ]);
}

static void gpio_setup(void)
{
    rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOD);
    gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO7);
	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO5);
    gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO6);
}

int main(void) {
    clock_setup();
    gpio_setup();
    int i;
    volatile float a,b,c,pi;
    a = 0.5;
    b = 0.75;
    c = 0.25;
    pi = 0;
    gpio_set(GPIOB, GPIO7);
    gpio_set(GPIOD, GPIO5);

    for (i = 1; i <= NPTS; ++i) {
        pi += a/((i-b)*(i-c));
    }

    gpio_clear(GPIOD, GPIO5);
    gpio_clear(GPIOB, GPIO7);
    while(1) {
    }
}
