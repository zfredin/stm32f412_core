## pi calculation test
This test follows the standard [CBA pi benchmark](https://gitlab.cba.mit.edu/pub/pi). Instead of setting up a real-time clock and computing the FLOPS on-board and reporting via serial terminal, I added a few GPIO toggles to flip a pin immediately prior to and after the pi calculation `for` loop. I also reduced the iteration count to 1,000,000 to reduce the wait time for the test. I then set the MDO3024 to 4 s/div capture and watched the GPIO pin in single-shot mode:

![pi capture](pi_capture.png)

The measurement shows a total calculation time of 12.91 s, including a [for these purposes] negligible delay to toggle the GPIOs. The FPU is set up with flags `-mfloat-abi=hard -mfpu=fpv4-sp-d16`, while the compiler includes `-O3`. Based on the pi benchmark calculation rate of 5 FLOPS per cycle iteration, this test suggests the STM32F412 is running at (5 * 1E6) / (12.91) = 0.387 MFLOPS. At the current 84 MHz clock speed, this suggests the pi calculation is taking (8.4E7) / (1E6 / 12.91) = 1084 clock cycles per iteration. This is a few orders of magnitude high (based on 1 clock cycle add/subtract/multiply, 14 clock cycle divide), suggesting the FPU isn't cooperating.

After re-consulting the data sheet, I changed the calculation variables from `double` to `float` since the Cortex M4F FPU only does single-precision math. This gave me a more favorable result; note the changed horizontal scale:

![pi capture2](pi_capture2.png)

Using the above calculations, this suggests the STM32F412 is running at 12.79 MFLOPS, or 33 clock cycles per iteration. Adding the `-ffast-math` option to `rules.mk` did not change this number. The datasheet suggests a typical current consumption of 22.7 mA at 84 MHz (with all peripherals on); at 3.3 VDC, this equates to 74.9 mW, or 0.171 GFlops/W.