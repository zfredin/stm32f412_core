# USART benchmark
This test uses a pair of dev boards to test sending serial data packets between nodes. The Nucleo boards have a labeled USART port which corresponds to USART3; by default, this port is jumpered to the ST-LINK chip for mbed debugging. To avoid having to remove jumpers, we'll use USART1, available on PA15 (TX) and PB3 (RX). Per the Nucleo schematic, these are broken out on the top right black double-row header (CN7) as pin 9 (PA15) and pin 15 (PB3). These are labeled as the first I2S_B pin and the second SPI_B pin, respectively. Orient the boards back-to-front so the jumpers reach, as shown here:

![usartbenchmark_setup](usartbenchmark_setup.jpg)

Initial tests suggest that USART1 maxes out at 5.25 Mbit/s; this is currently limited by the 84 MHz clock rate, since the advertised maximum speed is 6.25 Mbit/s and (84/100) * 6.25 = 5.25. Using `0x55` as a data payload with a single stop bit (i.e. 9 bits total), along with the `usart_irq` example from `libopencm3-examples` (modified slightly), I measured a round-trip message time of 6.05 microseconds:

![usartbenchmark_roundtrip](usartbenchmark_roundtrip.png)
