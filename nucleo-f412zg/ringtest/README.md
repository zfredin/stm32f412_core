# Ring Test
This ring test uses PD5 and PD6. It's best to solder a loop of bare wire between the two pads, as they're next to each other on the Nucleo board and a loop allows easy hookup of an oscilloscope probe:

![ringtest_setup](ringtest_setup.jpg)

I set the system clock to high-speed internal (HSI) running at 84 MHz, and used libopencm3's `gpio_get`, `gpio_set`, `and gpio_clear` to run the test:

![ringtest](ringtest.png)

1.97 MHz is good; I believe this could improve to 2.35 MHz if the HSI ran at the full rated STM32F412 speed of 100 MHz. libopencm3 doesn't have a built-in configuration function for this clock speed so this will take more investigation.
