## STM32F412 Nucleo Development board
The STM32F412 Nucelo board includes a few LEDs, user-accessible input and reset buttons, and a detachable ST-LINK programmer.
### getting st-link to work
To program the board using open-source Linux command-line tools, start by installing stlink following the instructions [here](https://github.com/texane/stlink/blob/master/doc/compiling.md). You'll likely need to install cmake and the libusb-dev:
```
sudo apt install cmake
sudo apt install libusb-1.0-0-dev
```
... then you can navigate to the stlink directory and run `make release`, and follow the rest of the instructions. I suggest installing the program system-wide by navigating to `build/release` and running:
```
sudo make install
sudo ldconfig
```
If everything works, you shouldn't see any errors. Test the program by running `st-util`. You should see something like:
```
st-util 1.5.1-38-gc3577b5
2019-10-01T20:06:32 WARN usb.c: Couldn't find any ST-Link/V2 devices
```
Plug your dev board in, using the USB plug connected to the detachable ST-LINK programmer. Run `st-link` again; the COM LED should turn green, and you should see something like:
```
st-util 1.5.1-38-gc3577b5
2019-10-01T20:09:30 INFO common.c: Loading device parameters....
2019-10-01T20:09:30 INFO common.c: Device connected is: F4 device, id 0x30006441
2019-10-01T20:09:30 INFO common.c: SRAM size: 0x40000 bytes (256 KiB), Flash: 0x100000 bytes (1024 KiB) in pages of 16384 bytes
2019-10-01T20:09:30 INFO gdb-server.c: Chip ID is 00000441, Core ID is  2ba01477.
2019-10-01T20:09:30 INFO gdb-server.c: Listening at *:4242...
```
Hooray! Okay, hit Ctrl-C to close the server.
### it's blinky time
Navigate to the `blink` directory and run `make`. You'll see a ton of libopencm3 stuff get built; if the process finishes without errors, you'll be left with a few new files in the `blink` directory, including `blink.elf`. Open up another terminal window and run `st-util` to start the gdb server back up, then return to your original terminal and run `arm-none-eabi-gdb blink.elf`. Connect to the ST-LINK board by typing `tar ext :4242`, then type `load` and `continue`. The whole routine should look like this:
```
GNU gdb (GNU Tools for Arm Embedded Processors 7-2018-q3-update) 8.1.0.20180315-git
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-pc-linux-gnu --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from blink.elf...done.
(gdb) tar ext :4242
Remote debugging using :4242
0x0800893c in ?? ()
(gdb) load
Loading section .text, size 0x2f0 lma 0x8000000
Start address 0x8000254, load size 752
Transfer rate: 919 bytes/sec, 752 bytes/write.
(gdb) continue
Continuing.
```
If everything went well, the red LED should have stopped blinking, and the blue LED should have started blinking. Yay!

If you run in to issues, the error message should point you in the right direction; it's likely a missing dependency, such as `arm-none-eabi-gdb`, `cmake`, `libusb-1.0-1-dev`, etc. Post a GitLab issue with your error and we'll help you get sorted!
