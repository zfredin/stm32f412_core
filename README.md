# STM32F412_core

Core development environment and examples for the STM32F412 Arm Cortex M4F chipset.

This environment makes use of the following tools:

[stlink](https://github.com/texane/stlink), an open-source command-line tool for interfacing with ST's STLINK chips (present on Discovery and Nucleo boards). I discuss installing this program in the [nucleo-f412zg directory](/nucleo-f412zg).

[libopencm3](https://github.com/libopencm3/libopencm3) and [libopencm3-examples](https://github.com/libopencm3/libopencm3-examples), an open-source firmware library for a variety of Cortex-M microcontrollers. The library has extensive documentation available [here](http://libopencm3.org/docs/latest/html/).

These tools allow us to use the STM32F412 platform without installing proprietary vendor-specific software or giving away our contact information. They are referenced in this repository as submodules from their respective homes on GitHub; that means you'll have a local copy on your system but updates will get pulled in from the maintained versions. In order for the submodules' content to pull in, run the following commands in the root folder after cloning this repo:

```
git submodule init
git submodule update
```

To update these submodules, navigate to their respective directories and run `git submodule update --remote`.
